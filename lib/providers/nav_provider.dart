import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:test_router/enums.dart';

class NavProvider {
  ///Singleton
  NavProvider._internal();
  static NavProvider instance = NavProvider._internal();

  // 導主頁方式之一
  void navToView(BuildContext context, NavType navType) {
    switch (navType) {
      case NavType.homeView:
        context.go("/home");
        break;
      case NavType.messageView:
        context.go("/message");
        break;
      case NavType.musicView:
        context.go("/music");
        break;
      case NavType.profileView:
        context.go("/profile");
        break;
    }
  }

  void init() {}
}
