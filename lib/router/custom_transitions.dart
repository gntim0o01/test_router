import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

// 轉場特效
class CustomTransitions {
  // 淡入淡出 效果
  static CustomTransitionPage fadeMode<T>({
    required BuildContext context,
    required GoRouterState state,
    required Widget child,
  }) {
    return CustomTransitionPage<T>(
      key: state.pageKey,
      child: child,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return FadeTransition(
          opacity: animation,
          child: child,
        );
      },
    );
  }
}
