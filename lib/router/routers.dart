import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:test_router/pages/home_sub/home_sub_view.dart';
import 'package:test_router/pages/message_sub/message_sub_view.dart';
import 'package:test_router/pages/nav/navigation_view.dart';
import 'package:test_router/pages/home/home_view.dart';
import 'package:test_router/pages/message/message_view.dart';
import 'package:test_router/pages/music/music_view.dart';
import 'package:test_router/pages/profile/profile_view.dart';
import 'package:test_router/pages/splash/splash_view.dart';
import 'package:test_router/router/custom_transitions.dart';

class Routers {
  static final GlobalKey<NavigatorState> rootKey = GlobalKey<NavigatorState>();

  static final GoRouter goRouter = GoRouter(
    routerNeglect: true,
    navigatorKey: rootKey,
    initialLocation: '/splash',
    routes: [
      GoRoute(
        path: '/splash',
        name: 'splash',
        parentNavigatorKey: rootKey,
        builder: (context, state) {
          setTitle('啟動頁');
          return const SplashView();
        },
      ),
      // 也可以這樣設置, 獨立頁設置
      // GoRoute(
      //   path: '/home/home_sub',
      //   pageBuilder: (context, state) {
      //     return const MaterialPage(fullscreenDialog: true, child: HomeSubView());
      //   },
      // ),
      ShellRoute(
        restorationScopeId: 'restoration_nav_bar',
        builder: (context, state, child) {
          return NavigationView(key: state.pageKey, contentView: child);
        },
        routes: [
          _withRoute(
            title: "home 頁面",
            name: 'home',
            view: const HomeView(),
            routes: [
              // 子頁設置
              GoRoute(
                path: 'home_sub',
                // 也可以這樣設置
                // builder: (context, state) {
                //   setTitle('Home Sub頁');
                //   return const HomeSubView();
                // },

                // 也可以這樣設置
                // 自定義跳轉動畫
                pageBuilder: (context, state) => CustomTransitions.fadeMode(
                  context: context,
                  state: state,
                  child: const HomeSubView(),
                ),
              ),
            ],
          ),
          _withRoute(
            title: "message 頁面",
            name: 'message',
            view: const MessageView(),
            routes: [
              // 子頁設置
              GoRoute(
                path: 'message_sub',
                builder: (context, state) {
                  setTitle('Message Sub頁');
                  return const MessageSubView();
                },
              ),
            ],
          ),
          _withRoute(
            title: "music 頁面",
            name: 'music',
            view: const MusicView(),
          ),
          _withRoute(
            title: "profile 頁面",
            name: 'profile',
            view: const ProfileView(userId: null),
            customPageBuilder: (context, state) {
              // 接收 參數 userId
              String? userId;
              if (state.extra != null) {
                var mapData = state.extra as Map<String, String>;
                if (mapData.containsKey("userId")) {
                  userId = mapData["userId"];
                }
              }

              return NoTransitionPage(
                key: state.pageKey,
                child: ProfileView(userId: userId),
              );
            },
          ),
        ],
      ),
    ],
  );

  //
  static Future goProfileView({String? id}) async {
    if (id == null) return null;
    return goRouter.push('/profile', extra: {'userId': id});
  }

  static GoRoute _withRoute({
    required String name,
    required Widget view,
    String? title,
    List<RouteBase>? routes,
    Page<dynamic> Function(BuildContext context, GoRouterState state)? customPageBuilder,
  }) {
    // 有自訂的的PageBuilder
    if (customPageBuilder != null) {
      return GoRoute(
        path: "/$name",
        name: name,
        routes: routes ?? [],
        pageBuilder: customPageBuilder,
      );
    }

    // 無自訂的的PageBuilder
    return GoRoute(
      path: "/$name",
      name: name,
      routes: routes ?? [],
      pageBuilder: (context, state) {
        setTitle("$title");
        return NoTransitionPage(key: state.pageKey, child: view);
      },
    );
  }

  // 設置 網頁title
  static setTitle(String value) {
    if (!kIsWeb) return;
    SystemChrome.setApplicationSwitcherDescription(ApplicationSwitcherDescription(
      label: value,
    ));
  }
}
