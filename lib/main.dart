import 'package:flutter/material.dart';
import 'package:test_router/providers/nav_provider.dart';
import 'package:test_router/router/routers.dart';
import 'package:url_strategy/url_strategy.dart';

void main() {
  setPathUrlStrategy(); // 去除#
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    init();
    super.initState();
  }

  void init() {
    NavProvider.instance.init();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      routerConfig: Routers.goRouter, // 路由設置
      debugShowCheckedModeBanner: false,
    );
  }
}
