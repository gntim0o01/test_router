import 'package:flutter/material.dart';
import 'package:test_router/router/routers.dart';

class MusicView extends StatefulWidget {
  const MusicView({super.key});

  @override
  State<MusicView> createState() => _MusicViewState();
}

class _MusicViewState extends State<MusicView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Music View")),
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            child: const Text("Music"),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                // 導頁方式之一
                // 傳送 參數
                Routers.goProfileView(id: "afckmxua");
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.blue,
                height: 30,
                width: 100,
                child: const Text("To Profile"),
              ),
            ),
          )
        ],
      ),
    );
  }
}
