import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SplashView extends StatefulWidget {
  const SplashView({super.key});

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  int count = 3;

  @override
  void initState() {
    autoToPage();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void autoToPage() {
    // 倒數計時
    Timer.periodic(const Duration(seconds: 1), (timer) {
      if (count == 0) {
        // 導頁方式之一(不堆疊)
        context.go("/home");
        timer.cancel();
        return;
      }
      count--;
      if (mounted) {
        // 畫面還存在時, 才刷新
        // 避免報錯
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Splash View")),
      backgroundColor: Colors.white,
      body: Container(
        alignment: Alignment.center,
        child: Text(
          "倒數 $count秒",
          style: const TextStyle(
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
