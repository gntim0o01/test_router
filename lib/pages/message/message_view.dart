import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class MessageView extends StatefulWidget {
  const MessageView({super.key});

  @override
  State<MessageView> createState() => _MessageViewState();
}

class _MessageViewState extends State<MessageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Message View")),
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            child: const Text("Message"),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                // 導頁方式之一(堆疊)
                context.go("/message/message_sub");
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.blue,
                height: 30,
                width: 100,
                child: const Text("Next"),
              ),
            ),
          )
        ],
      ),
    );
  }
}
