import 'package:flutter/material.dart';
import 'package:test_router/enums.dart';
import 'package:test_router/providers/nav_provider.dart';

class MessageSubView extends StatefulWidget {
  const MessageSubView({super.key});

  @override
  State<MessageSubView> createState() => _MessageSubViewState();
}

class _MessageSubViewState extends State<MessageSubView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Message Sub View")),
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            child: const Text("Message Sub"),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                // 導頁方式之一(不堆疊)
                NavProvider.instance.navToView(context, NavType.homeView);
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.blue,
                height: 30,
                width: 100,
                child: const Text("To Home"),
              ),
            ),
          )
        ],
      ),
    );
  }
}
