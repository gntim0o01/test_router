import 'package:flutter/material.dart';
import 'package:test_router/enums.dart';
import 'package:test_router/providers/nav_provider.dart';

class HomeSubView extends StatefulWidget {
  const HomeSubView({super.key});

  @override
  State<HomeSubView> createState() => _HomeSubViewState();
}

class _HomeSubViewState extends State<HomeSubView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Home Sub View")),
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            child: const Text("Home Sub"),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                // 導頁方式之一
                NavProvider.instance.navToView(context, NavType.musicView);
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.blue,
                height: 30,
                width: 100,
                child: const Text("To Music"),
              ),
            ),
          )
        ],
      ),
    );
  }
}
