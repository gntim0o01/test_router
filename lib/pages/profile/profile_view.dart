import 'package:flutter/material.dart';

class ProfileView extends StatefulWidget {
  final String? userId;
  const ProfileView({super.key, required this.userId});

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Profile View")),
      backgroundColor: Colors.white,
      body: Container(
        alignment: Alignment.center,
        child: Text(
          "Profile User Id: ${widget.userId}",
        ),
      ),
    );
  }
}
