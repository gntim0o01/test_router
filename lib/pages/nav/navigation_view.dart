import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

// 底下的Navigation Bar
class NavigationView extends StatefulWidget {
  final Widget contentView;
  const NavigationView({
    super.key,
    required this.contentView,
  });

  @override
  State<NavigationView> createState() => _NavigationViewState();
}

class _NavigationViewState extends State<NavigationView> {
  int currentIndex = 0; // 初始頁面
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: widget.contentView,
        ),
        const Divider(color: Colors.black26),
        Container(
          color: Colors.white,
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _navItem(icon: Icons.home, location: "/home"),
              _navItem(icon: Icons.message, location: "/message"),
              _navItem(icon: Icons.music_note, location: "/music"),
              _navItem(icon: Icons.person, location: "/profile"),
            ],
          ),
        ),
      ],
    );
  }

  Widget _navItem({
    required IconData icon,
    required String location,
    Function()? onTap,
  }) {
    final currentLaction = GoRouter.of(context).routeInformationProvider.value.location;
    final isSelected = currentLaction == location;
    return GestureDetector(
      onTap: () {
        onTap?.call(); // 觸發onTap
        context.go(location); // 跳頁, 導頁方式之一(不堆疊)
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        child: Icon(
          icon,
          color: isSelected ? Colors.black : Colors.grey,
          size: 25,
        ),
      ),
    );
  }
}
