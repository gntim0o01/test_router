import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Home View")),
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            child: const Text("Home"),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                // 導頁方式之一(堆疊)
                context.push("/home/home_sub");
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.blue,
                height: 30,
                width: 100,
                child: const Text("Next"),
              ),
            ),
          )
        ],
      ),
    );
  }
}
